import {Component, Input, OnInit} from '@angular/core';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {RegistrationCoursesComponent} from '../registrations-courses/registrations-courses.component';

@Component({
  selector: 'app-registrations-course-class',
  templateUrl: './registrations-course-class.component.html',
  styleUrls: ['./registrations-course-class.component.scss']
})

export class RegistrationsCourseClassComponent implements OnInit {
  public course: any;
  public courseClass: any;
  public sections;
  public selectedSection;
  public isLoading = true;
  public disabled = false;
  public lastError;
  constructor(public _bsModalRef: BsModalRef,
              private _currentRegService: CurrentRegistrationService) {
  }

  ngOnInit() {
    this.courseClass = this.course.courseClass;
    this._currentRegService.getClassSections(this.courseClass.id).then(res => {
      this.sections = res;
      this.isLoading = false;
      });
    }

  selectClassSection(selectedSection: any): void  {
    this.lastError = null;
    this.course.section = selectedSection.section;
    this.course.sectionName = selectedSection.name;
    this.course.courseClass.instructors = selectedSection.instructors;
    this._currentRegService.registerForCourse(this.course).then(() => {
      this.course.isRegisteredCourse = true;
      this._bsModalRef.hide();
      this.disabled = false;
    }).catch( err => {
      // log error to console
      this.disabled = false;
      console.log(err);
      this.lastError = err;
    });
  }
}
